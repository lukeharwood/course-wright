import './css/style.css';
import './css/core.css';
import {Route, Routes} from "react-router-dom";
import Layout from "./outlets/Layout";
import NotFound from "./pages/not-found/404";
import Home from "./pages/home/Home";
import SignIn from "./pages/forms/SignIn";
import RefreshHandler from "./outlets/RefreshHandler";
import AuthorizeUser from "./outlets/AuthorizeUser";
import Dashboard from "./pages/dashboard/Dashboard";
import CreateAccount from "./pages/forms/CreateAccount";
import Section from "./pages/section/Section";
import {Path} from "./data/paths";
import CreateClassPage from "./pages/section/CreateClassPage";
import CreateCoursePage from "./pages/course/CreateCoursePage";
import CoursePage from "./pages/course-page/CoursePage";
import {AnimatePresence} from "framer-motion";

function App() {
  return (
      <AnimatePresence>
          <Routes>
              <Route path={"/"} element={<Layout />}>
                  <Route element={<RefreshHandler/>}>
                      <Route index element={<Home />} />
                  </Route>
                  <Route path={"sign-in"} element={<SignIn />} />
                  <Route path={"create-account"} element={<CreateAccount />}/>
                  {/* protected routes here */}
                  <Route element={<RefreshHandler />}>
                      <Route element={<AuthorizeUser />}>
                          <Route path={"dashboard"} element={<Dashboard />} />
                          <Route path={Path.Dashboard.CLASS_CREATE_ID(":id?")} element={<CreateClassPage />} />
                          <Route path={Path.Dashboard.COURSE_CREATE} element={<CreateCoursePage />} />
                          <Route path={Path.Dashboard.COURSE_VIEW(":id")} element={<CoursePage />} />
                          {/*<Route path={"course/:type/:id"} element={<CoursePage/> } />*/}
                      </Route>
                  </Route>
                  <Route path={"*"} element={<NotFound />} />
              </Route>
          </Routes>
      </AnimatePresence>
  );
}

export default App;
