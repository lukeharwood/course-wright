import {useNavigate, useParams} from "react-router-dom";
import {Path} from "../../data/paths";
import {IoArrowBackCircleOutline} from "react-icons/io5";
import {Tooltip} from "@mui/material";
import { motion } from 'framer-motion';
import CreateClassPage from "./CreateClassPage";

const Section = () => {
    const { courseId } = useParams();
    console.log(courseId);
    const navigate = useNavigate();

    const handleNavToDashboard = () => {
        navigate(Path.DASHBOARD);
    }
    return (
        <div className={"flex-col align-center"}>

        </div>
    );
}

export default Section;