import './section.css'
import {useNavigate, useParams} from "react-router-dom";
import {
    Checkbox,
    FormControl,
    FormControlLabel,
    Input,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Tooltip
} from "@mui/material";
import ColorPicker from "../../components/input/ColorPicker";
import {useEffect, useState} from "react";
import AsyncSelect from "react-select/async";
import TabPane from "../../components/container/TabPane";
import NewClassOptionsPanel from "./NewClassOptionsPanel";
import FindCoursePanel from "./FindCoursePanel";
import toast from "react-hot-toast";
import {useCourses} from "../../hooks/useCourses";
import {Path} from "../../data/paths";
import {BiArrowBack} from "react-icons/bi";
import { motion } from 'framer-motion';

const CreateClassPage = () => {
    const { id } = useParams();
    const [allow, setAllow] = useState(false);
    const [courseId, setCourseId] = useState(id ? id : "");
    const [course, setCourse] = useState({});
    const { getCourse } = useCourses();
    const navigate = useNavigate();

    const handleSubmit = () => {

    }

    useEffect(() => {
        if (courseId) getCourse(courseId).then(c => {
            setAllow(c);
            setCourse(c);
            navigate(Path.Dashboard.CLASS_CREATE_ID(c._id))
        }).catch(err => {
            toast.error(err)
            navigate(Path.Dashboard.CLASS_CREATE)
        });
    }, [courseId])

    return (
        <div className={"create-new-section"}>
            <Tooltip placement={"top"} title={"Return to Dashboard"}>
                <motion.button onClick={() => navigate(Path.DASHBOARD)} whileHover={{ scale: 1.05 }} className={"return-to-dash-btn"}><BiArrowBack size={40}/></motion.button>
            </Tooltip>
            <TabPane allowPrevious={true} nextButton allowNext={allow} numbered tabHeader tabLabels={["Select Course", "Create New Class"]}>
                <FindCoursePanel course={courseId} setCourse={setCourseId} />
                <NewClassOptionsPanel course={course} onSubmit={handleSubmit}/>
            </TabPane>
        </div>
    );
}

export default CreateClassPage;