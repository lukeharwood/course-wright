import {useNavigate, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import { motion } from 'framer-motion';
import AsyncSelect from "react-select/async";
import {
    Checkbox,
    FormControl,
    FormControlLabel,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    TextField, Tooltip
} from "@mui/material";
import ColorPicker from "../../components/input/ColorPicker";
import toast from "react-hot-toast";
import {Tag} from "../../components/labeling/Tags";
import {Defaults} from "../../data/enums";
import {ControlBar} from "../../components/navbar/ControlBar";
import {autoEntranceTranslate, linkSharingTranslate} from "../../utils/messaging/courseOptionTranslator";
import {useSections} from "../../hooks/useSections";
import { AiOutlinePlus } from 'react-icons/ai'
import {Path} from "../../data/paths";
import {MdRemove} from "react-icons/md";
import SectionHeader from "../../components/labeling/SectionHeader";

const NewClassOptionsPanel = ({ course }) => {
    const navigate = useNavigate();

    const [numSections, setNumSections] = useState( 2);
    const [multipleSections, setMultipleSections] = useState(false);
    const [sectionNumbers, setSectionNumbers] = useState([Defaults.SECTION_NUMBERS[0]]);
    const [colors, setColors] = useState([Defaults.SECTION_COLORS[0]]);
    const [linkSharing, setLinkSharing] = useState(false);
    const [autoEntrance, setAutoEntrance] = useState(false);
    const [teachers, setTeachers] = useState([]);
    const { addSections } = useSections();

    const loadOptions = (inputValue, callback) => {
        callback([{ label: "Helper", value: "id" }])
    };

    const handleCodeChange = (e, i) => {
        setSectionNumbers((prev) => {
            let sns = [...prev];
            return sns.map((v, li) => {
                if (li === i) {
                    return e.target.value;
                }
                return v;
            })
        })
    }

    const handleColorChange = (e, i) => {
        setColors((prev) => {
            const clrs = [...prev];
            clrs[i] = e.target.value;
            return clrs.map((v, li) => {
                if (li === i) {
                    return e.target.value;
                }
                return v;
            })
        })
    }

    const handleSubmit = async () => {
        const obj = {
            course: course._id,
            numbers: sectionNumbers.splice(0, multipleSections ? numSections: 1),
            colors: colors.splice(0, multipleSections ? numSections: 1),
            linkSharingEnabled: linkSharing,
            autoEntrance,
            teachers,
        }
        const id = toast.loading("Creating new sections...", { id: "create-sections"});
        addSections(obj).then(r => {
            toast.success("Sections Created.", { id })
            navigate(Path.DASHBOARD_SECTIONS);
        }).catch((err) => toast.error(err, { id }));
    }

    const handleCreateSectionNumber = () => {
        if (sectionNumbers.length < Defaults.SECTION_NUMBERS.length) {
            setSectionNumbers((prev) => {
                return [...prev, Defaults.SECTION_NUMBERS[prev.length]];
            })
            setColors((prev) => {
                return [...prev, Defaults.SECTION_COLORS[prev.length]];
            })
        } else {
            toast.error("Max section numbers reached.");
        }
    }

    const handleRemoveSection = (i) => {
        setSectionNumbers(prev => {
            return prev.filter((s, index) => index !== i);
        })

        setColors(prev => {
            return prev.filter((c, index) => index !== i)
        })
    }

    return (
        <>
            <div>
                {
                    course && <p style={{ boxShadow: `${course.color} 2px 2px 5px -2px`}} className={"no-select section-course-name-header"}>{ course?.name } <Tag color={ course?.color } text={ course?.code } /></p>
                }
            </div>
            <div className={"section-options-panel"}>
                <section className={"section-block section-codes"}>
                    {
                        sectionNumbers.map((n, i) => {
                            return (
                                <div className={"section-code-card"} key={i}>
                                    {
                                        sectionNumbers.length > 1 &&
                                        <Tooltip placement={"top"} title={"Remove Section"} >
                                            <div><motion.div onClick={() => handleRemoveSection(i)} whileHover={{ scale: 1.05}}><MdRemove className={"remove-section-btn"} size={20}/></motion.div></div>
                                        </Tooltip>
                                    }
                                    <TextField className={"section-code"} value={n} onChange={ (e) => handleCodeChange(e, i) } style={{ margin: "1rem 0"}} sx={{ width: "70px"}} id="course-subject" label={`Section ${i + 1} ID`} variant="standard" />
                                    <ColorPicker size={40} color={colors[i]} onChange={(e) => handleColorChange(e, i) } />
                                </div>
                            )
                        })
                    }
                    {
                        colors.length < Defaults.SECTION_COLORS.length && <Tooltip placement={"top"} title={"Add Class Section"}>
                            <div className={"flex align-center h-min"}>
                                <motion.div onClick={ handleCreateSectionNumber } className={"new-section-btn"}  whileHover={{ rotate: 90, borderRadius: "50%" }} ><AiOutlinePlus  size={20}/></motion.div>
                            </div>
                        </Tooltip>
                    }

                </section>
                <section className={"section-block meta-data-new-section"}>
                    <FormControl sx={{ width: "170px"}} >
                        <InputLabel id="num-sections-input">Link Sharing</InputLabel>
                        <Select
                            labelId="num-sections-input"
                            id="demo-simple-select"
                            value={ linkSharing }
                            label="Auto Entrance"
                            onChange={(e) => setLinkSharing(e.target.value)}
                        >
                            <MenuItem value={false}>Disabled</MenuItem>
                            <MenuItem value={true}>Enabled</MenuItem>
                        </Select>
                        <FormHelperText>{ linkSharingTranslate(linkSharing)}</FormHelperText>

                    </FormControl>
                    <FormControl sx={{ width: "170px"}} >
                        <InputLabel id="num-sections-input">Auto Entrance</InputLabel>
                        <Select
                            labelId="num-sections-input"
                            id="demo-simple-select"
                            value={ autoEntrance }
                            label="Auto Entrance"
                            onChange={(e) => setAutoEntrance(e.target.value)}
                        >
                            <MenuItem value={false}>Disabled</MenuItem>
                            <MenuItem value={true}>Enabled</MenuItem>
                        </Select>
                        <FormHelperText>{ autoEntranceTranslate(autoEntrance)}</FormHelperText>

                    </FormControl>
                </section>
                <section className={"teachers-section section-block"}>
                    <AsyncSelect placeholder={"Add Teachers"} cacheOptions loadOptions={loadOptions} defaultOptions />
                    <ul>

                    </ul>
                </section>
                <section className={"sharing section-block"}>
                    <ControlBar align={"right"}>
                        <button onClick={ handleSubmit } className={"section-create-button"}>Create Class</button>
                    </ControlBar>
                </section>
            </div>
        </>

    );
}

export default NewClassOptionsPanel;