import {useNavigate, useParams} from "react-router-dom";
import {useCourses} from "../../hooks/useCourses";
import {useEffect, useState} from "react";
import toast from "react-hot-toast";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {Tag} from "../../components/labeling/Tags";

const FindCoursePanel = ({ course, setCourse }) => {
    const { getCourses } = useCourses();
    const navigate = useNavigate();

    const [courses, setCourses] = useState([]);

    useEffect(() => {
        getCourses().then((c) => {
            setCourses(c);
        }).catch((err) => toast.error(err));
    }, [])

    return (
        <section className={"flex justify-center"}>
            <FormControl sx={{ marginTop: "1rem", width: "min(500px, 90vw)"}}>
                <InputLabel id={"sharing-label"}>My Courses</InputLabel>

                <Select
                    labelId={"sharing-label"}
                    id={"sharing"}
                    label={"My Courses"}
                    value={ courses?.length > 0 ? course : "" }
                    onChange={(e) => setCourse(e.target.value)}
                >
                    {
                        courses && courses.map((c) => <MenuItem key={c._id} value={c._id}><div className={"dropdown-search-item"}> <p className={"dropdown-search-item-text"}>{ c.name }</p><Tag margin={"0 0 0 1rem"} color={c.color} text={ c.code } /></div></MenuItem>)
                    }
                </Select>

                <div>

                </div>
            </FormControl>
        </section>
    );
}

export default FindCoursePanel;