import './forms.css'
import Logo, {LogoText} from "../home/Logo";
import {AiFillEye, AiFillEyeInvisible, AiOutlineMail} from 'react-icons/ai'
import { FcGoogle } from 'react-icons/fc';
import {useLocation, useNavigate} from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import {useEffect, useRef, useState} from "react";
import useMemoryState from "../../hooks/useMemoryState";
import toast from "react-hot-toast";
import axios from "../../api/axios";
import {ServerPath, SIGN_IN_URL} from "../../data/paths";

const SignIn = () => {
    const navigate = useNavigate();
    const { setAuth } = useAuth()
    const emailRef = useRef()
    const [email, setEmail] = useMemoryState("", "email")
    const [password, setPassword] = useState("")
    const [pwdVisible, setPwdVisible] = useState(false)
    const location = useLocation()
    const from = location?.state?.from.pathname || '/'
    const submitRef = useRef();

    useEffect(() => {
        emailRef.current.focus()
    }, [])

    const togglePasswordVisible = () => {
        setPwdVisible((p) => {
            return !p;
        })
    }

    const handleSignIn = async (e) => {
        e.preventDefault();
        const id = toast.loading("Signing you in...", { id: "sign-in"})
        try {
            const response = await axios.post(
                ServerPath.SIGN_IN_URL,
                JSON.stringify({ email, password }),
                {
                    headers: {"Content-Type": "application/json"},
                    withCredentials: true
                }
            )
            setEmail("")
            setPassword("")

            const { user, accessToken } = response.data
            setAuth({ user, accessToken })
            toast.success(`Welcome ${ user.firstName }!`, { id: id })
            navigate(from, { replace: true })
        } catch (err) {
            if (!err?.response) {
                toast.error('No Server Response', { id: id })
            } else if (err.response?.status === 400) {
                toast.error("Missing Username or Password", { id: id })
            } else if (err.response?.status === 401) {
                toast.error("Incorrect Email or password.", { id: id })
                setPassword("")
            } else {
                toast.error("Login Failed", { id: id })
            }
        }
    }

    return (
        <div className={"sign-in-page"}>
            <LogoText />
            <form onSubmit={handleSignIn} className={"sign-in sm-container no-select"}>
                <h1 className={"header fw-thin fs-md"}>Welcome Back!</h1>
                <div className={"icon-group fs-md"}>
                    <AiOutlineMail className={"icon-group-icon"} size={30}/>
                    <input value={email} onChange={(e) => setEmail(e.target.value)} ref={emailRef} placeholder={"email"} className={"input fs-md full"} type={'text'} required/>
                </div>
                <div className={"icon-group"}>
                    {
                        pwdVisible ? <AiFillEye onClick={ togglePasswordVisible } className={"icon-group-icon clickable"} size={30}/> : <AiFillEyeInvisible onClick={ togglePasswordVisible } className={"icon-group-icon clickable"} size={30}/>
                    }
                    <input value={password} onChange={(e) => setPassword(e.target.value)} placeholder={"password"}  className={"input fs-md full"} type={pwdVisible ? "text": "password"} required/>
                </div>
                <div onClick={ () => submitRef.current.click() } className={"form-button form-button-inverse"}>Sign In</div>
                <input ref={submitRef} type={'submit'} style={{ display: "none" }} />
                <div className={"google no-select"}>
                    <div className={"icon-group"}>
                        <FcGoogle className={"icon-group-icon"} size={30}/>
                        <p>Sign in with Google</p>
                    </div>
                </div>
                <p>Don't have an account? <span className={"linkable"} onClick={() => {navigate('/create-account');}}> Create Account</span></p>


            </form>


        </div>
    )
}

export default SignIn;