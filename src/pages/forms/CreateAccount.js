import './forms.css'
import Logo, {LogoText} from "../home/Logo";
import { AiOutlineMail } from 'react-icons/ai'
import { RiLockPasswordLine, RiLockPasswordFill, RiFileUserLine, RiFileUserFill } from 'react-icons/ri'
import { FcGoogle } from 'react-icons/fc';
import {useLocation, useNavigate} from "react-router-dom";
import toast from "react-hot-toast";
import {useEffect, useRef, useState} from "react";
import useMemoryState from "../../hooks/useMemoryState";
import useAuth from "../../hooks/useAuth";
import {SIGN_IN_URL} from "../../data/paths";
import axios from "../../api/axios";
import emailValidator from 'email-validator';

const CreateAccount = () => {

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const emailRef = useRef();
    const [password, setPassword] = useState("");
    const [rePassword, setRePassword] = useState("");
    const [validPwd, setValidPwd] = useState(false);
    const [validCPwd, setValidCPwd] = useState(false);
    const pwdRef = useRef();
    const rePwdRef = useRef();
    const submitRef = useRef();

    const navigate = useNavigate();

    const validInput = () => {
        if (!emailValidator.validate(email)) {
            setEmail("");
            emailRef.current.focus();
            toast.error("Invalid Email Address.");
            return false;
        }

        if (!validPwd) {
            toast.error("Password must have at least 8 characters.");
            return false;
        }

        if (!validCPwd) {
            toast.error("Passwords must match.");
            return false;
        }
        return true;
    }

    const clearMemoryState = () => {

    }

    useEffect(() => {
        pwdRef.current.oninvalid = (e) => e.target.setCustomValidity("Password must have at least 8 characters.")
        pwdRef.current.oninput = (e) => e.target.setCustomValidity("")
        rePwdRef.current.oninvalid = (e) => e.target.setCustomValidity("Passwords must match.")
        rePwdRef.current.oninput = (e) => e.target.setCustomValidity("")
        console.log("HERE");
        // eslint-disable-next-line
    }, [])


    const handleCreateAccount = async (e) => {
        e.preventDefault();
        if (!validInput()) return;
        const id = toast.loading("Creating New Account...", { id: "create-account"})
        try {
            await axios.post(
                "/create-account", JSON.stringify({
                    firstName, lastName, email, password, confirmPassword:rePassword
                }),
                {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                }
            )
            // successful, so clear from memory
            clearMemoryState()
            toast.success("Account Created!", { id: id })
            navigate("/sign-in")
        } catch (e) {
            if (!e?.response) {
                toast.error("No Server Response.", { id: id })
            } else if (e?.response.status === 409) {
                setEmail("")
                toast.error("Email already exists.", { id: id })
            } else if (e?.response.status === 403) {
                toast.error(e?.message, { id: id })
            } else {
                toast.error("Failed to create profile... please try again.", { id: id })
            }
        }
    }

    const listenForSubmit = (e) => {
        if (e.key === "Enter") {
            // submitRef.current.click();
        }
    }

    const onPasswordChange = (e) => {
        setPassword(e.target.value);
        setValidPwd(e.target.value.length >= 7);
    }

    const onConfirmPasswordChange = (e) => {
        setRePassword(e.target.value);
        setValidCPwd(validPwd && password === e.target.value);
    }

    return (
        <div className={"sign-in-page "}>
            <LogoText />
            <form onSubmit={handleCreateAccount} className={"sign-in sm-container no-select"}>
                <h1 className={"header fw-thin fs-md"}>{ firstName.trim() !== "" ? `Welcome ${firstName.trim()}!` : "Welcome!"}</h1>
                <div className={"icon-group fs-md"}>
                    <RiFileUserLine className={"icon-group-icon"} size={30}/>
                    <input value={firstName} onChange={(e) => setFirstName(e.target.value)} placeholder={"first name"} className={"input fs-md full"} type={'text'} required/>
                </div>
                <div className={"icon-group fs-md"}>
                    <RiFileUserFill className={"icon-group-icon"} size={30}/>
                    <input value={lastName} onChange={(e) => setLastName(e.target.value)} placeholder={"last name"} className={"input fs-md full"} type={'text'} required/>
                </div>
                <div className={"icon-group fs-md"}>
                    <AiOutlineMail className={"icon-group-icon"} size={30}/>
                    <input ref={emailRef} value={email} onChange={(e) => setEmail(e.target.value)} placeholder={"email"} className={"input fs-md full"} type={'text'} required/>
                </div>
                <div className={"icon-group"}>
                    <RiLockPasswordLine className={"icon-group-icon"} size={30}/>
                    <input ref={pwdRef} style={{ outline: validPwd ? "green 1px solid": "none"}}  value={password} onChange={onPasswordChange} placeholder={"password"} className={"input fs-md full"} type={'password'} required/>
                </div>
                <div className={"icon-group"}>
                    <RiLockPasswordFill className={"icon-group-icon"} size={30}/>
                    <input ref={rePwdRef} style={{ outline: validCPwd ? "green 1px solid": "none"}} value={rePassword} onChange={onConfirmPasswordChange} placeholder={"confirm password"} className={"input fs-md full"} type={'password'} required/>
                </div>
                <div onClick={() => submitRef.current.click()} className={"form-button form-button-inverse"}>Create</div>
                <p>Already have an account? <span className={"linkable"} onClick={() => {navigate('/sign-in');}}>Sign In</span></p>
                <input type={'submit'} ref={submitRef} style={{ display: "none"}}></input>
            </form>
        </div>
    )
}

export default CreateAccount;