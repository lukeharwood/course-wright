import {useRef, useState} from "react";
import {validLink} from "../../utils/regex";

const LinkInput = ({ onSubmit, handleClose }) => {

    const [title, setTitle] = useState("");
    const [url, setUrl] = useState("");

    const handleSubmit = () => {
        onSubmit(title, url);
        handleClose();
    }

    const handleLinkClick = () => {
        const e = document.createElement('a');
        e.href = url;
        e.target = "__blank";
        e.click();
    }

    return (
        <div className={"link-resource-modal"}>
            <h3>Add Link</h3>
            <input value={title} onChange={(e) => setTitle(e.target.value)} className={"link-resource-input link-resource-input-title"} placeholder={"Title (Optional)"}/>
            <div className={"input-group"}>
                <input value={url} onChange={(e) => setUrl(e.target.value)} className={`link-resource-input-url link-resource-input ${validLink(url) ? "valid-url" : ""}`} placeholder={"URL"} />
                <button onClick={handleLinkClick} disabled={!validLink(url)} className={"text-btn"}>Test</button>
            </div>
            <button disabled={!validLink(url)} onClick={handleSubmit} className={"link-resource-add-btn"}>Add</button>
        </div>
    );
}

export default LinkInput;