import {parseFileType} from "../../utils/regex";
import {getIconFromResourceType} from "../../utils/icons/iconsUtils";
import { AiOutlineDownload } from 'react-icons/ai';
import {useEffect, useState} from "react";
import {objectOrEmptyString} from "../../utils/inputConversion";
import {EditableText} from "../../components/input/TextField";

const DirectoryItem = ({ item }) => {

    const [mouseOver, setMouseOver] = useState(false);
    const [editMode, setEditMode] = useState(false);


    const handleNameChange = () => {

    }

    const handleClick = (e) => {
        e.stopPropagation();
    }

    return (
        <div onMouseEnter={() => setMouseOver(true)} onMouseLeave={() => setMouseOver(false)} onClick={handleClick} className={"directory-item"}>
            <EditableText value={item.name} onValueChange={handleNameChange} regex={/.*/} textClassName={""} />
            <div className={"icon-holder"}>
                {
                    (item.type === "file" && mouseOver) ? <AiOutlineDownload size={30} /> : getIconFromResourceType(item)
                }
            </div>
        </div>
    );
}

export default DirectoryItem;