import {useParams} from "react-router-dom";
import {useCourses} from "../../hooks/useCourses";
import {useEffect, useState} from "react";
import toast from "react-hot-toast";
import {Tag} from "../../components/labeling/Tags";
import './course-page.css'
import DirectoryCard from "./DirectoryCard";
import {Block, ControlBar, ControlItem} from "../../components/navbar/ControlBar";
import {
    AiFillFile,
    AiFillProfile, AiFillQuestionCircle,
    AiOutlineCloudUpload,
    AiOutlineFolderAdd,
    AiOutlineForm, AiOutlineLink,
    AiOutlineUpload
} from "react-icons/ai";
import Modal from "../../components/modal/Modal";
import LinkInput from "./LinkInput";
const CoursePage = () => {
    // course id to view
    const { id } = useParams();
    const { getCourse } = useCourses();
    const [course, setCourse] = useState({});
    const [selectedDir, setSelectedDir] = useState("");
    const [focus, setFocus] = useState("");
    const [resourceModal, setResourceModal] = useState(false);
    const [linkModal, setLinkModal] = useState(false);

    const handleFocus = () => {
        setFocus("");
    }


    useEffect(() => {
        getCourse(id).then(c => setCourse(c)).catch((err) => err && toast.error(err))
        window.addEventListener('click', handleFocus);
        return () => {
            window.removeEventListener('click', handleFocus);
        }
    }, [])

    const handleNewResource = () => {
        setResourceModal(true)
    }

    const handleNewResourcePage = () => {
        setResourceModal(false);
    }

    const handleNewAssignment = () => {
        setResourceModal(false);
    }

    const handleNewQuiz = () => {
        setResourceModal(false);
    }

    const handleNewLink = () => {
        setResourceModal(false);
        setLinkModal(true);
    }

    const handleNewFile = () => {
        setResourceModal(false);
    }

    return (
        <div className={"course-page"}>
            <Modal open={resourceModal} handleClose={() => setResourceModal(false)} preventAutoClose={false}>
                <div className={"course-new-resource no-select"}>
                    <div onClick={handleNewResourcePage}>Resource Page <AiFillFile size={30}/></div>
                    <div onClick={handleNewAssignment}>Assignment <AiFillProfile size={30} /></div>
                    <div onClick={handleNewQuiz}>Quiz <AiFillQuestionCircle size={30} /></div>
                    <div onClick={handleNewLink}>Link <AiOutlineLink size={30} /></div>
                    <div onClick={handleNewFile}>File <AiOutlineCloudUpload size={30} /></div>
                </div>
            </Modal>

            <Modal open={linkModal} handleClose={() => setLinkModal(false) }>
                <LinkInput onSubmit={() => {}} handleClose={() => setLinkModal(false)} />
            </Modal>


            <div style={{ boxShadow: `${course.color} 5px 5px 0 0`}} className={"course-name-header-container"}>
                <h1 className="course-name-header">{ course.name }</h1>
                <Tag color={course.color} text={course.code} />
                <hr className={"text-white"}/>
            </div>
            <div className={"directory-toolbar"}>
                <ControlBar align={"right"}>
                    <Block>
                        <ControlItem disabled tooltip={"Upload File"}><AiOutlineUpload size={25} /></ControlItem>
                        <ControlItem onClick={handleNewResource} tooltip={"Create new Resource"}><AiOutlineForm size={25} /></ControlItem>
                        <ControlItem tooltip={"Create new Directory"}><AiOutlineFolderAdd size={25} /></ControlItem>
                    </Block>
                </ControlBar>
            </div>
            <div className={"course-page-directories-view"}>
                <DirectoryCard color={course.color} setFocus={setFocus} focus={focus} dir={{ name: "Week 1", items: [{type: "file", name: "Helper.txt"}, { type: "file", name: "manual.pdf" }, { type: "file", name: "worksheet.md" }, { type: "page", name: "How Computers Work"}]}} />
            </div>
        </div>
    );
}

export default CoursePage;