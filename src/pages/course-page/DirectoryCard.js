import {useEffect, useState} from "react";
import {AiFillCaretDown, AiFillCaretUp} from "react-icons/ai";
import DirectoryItem from "./DirectoryItem";
import { motion } from 'framer-motion';
import toast from "react-hot-toast";

const DirectoryCard = ({ focus, setFocus, dir, color }) => {
    const { name, items } = dir;
    const [expanded, setExpanded] = useState(false);

    return (
        <div style={{ outline: focus === "id" ? `${color} 2px solid`: ""}} className={"directory-card no-select"} onClick={(e) => {
            setFocus("id")
            e.stopPropagation();
        }}>
            <div onClick={() => setExpanded(!expanded)} className={"directory-card-header"}>
                {
                    expanded ? <AiFillCaretUp size={20}/> : <AiFillCaretDown size={20}/>
                }
                <h3 className={"directory-card-name"}>{name}</h3>
            </div>
            {
                expanded && items.map((i, index) => <DirectoryItem key={index} item={i}/>)
            }
            {
                expanded && items.length === 0 && <p className={"no-items-text"}>No items to display</p>
            }
        </div>
    );
}

export default DirectoryCard;