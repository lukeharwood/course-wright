import './dashboard.css'
import CourseDisplay from "./components/course/CourseDisplay";
import {useState} from "react";
import ClassDisplay from "./components/class/ClassDisplay";
import Modal from "../../components/modal/Modal";
import DashboardProvider from "../../providers/DashboardProvider";
import SideDashboardNavBar from "./components/SideDashboardNavBar";
import {Divider} from "@mui/material";
import { motion } from 'framer-motion';

const Dashboard = () => {
    return (
        <DashboardProvider>
            <div className={"dashboard-page"}>
                <CourseDisplay />
                <ClassDisplay />
            </div>
        </DashboardProvider>
    );
}

export default Dashboard;