import {MdCreateNewFolder} from "react-icons/md";
import {useState} from "react";
import Modal from "../../../../components/modal/Modal";
import { CourseInfoPopupEditable } from "./CourseInfoPopup";
import {Tooltip} from "@mui/material";

const CourseOptionsButton = () => {
    const [open, setOpen] = useState(false);
    const handleButtonClicked = () => {
        setOpen(true);
    }

    return (
        <>
            <Modal preventAutoClose open={open} handleClose={() => setOpen(false)}>
                <CourseInfoPopupEditable edit={false} onClose={() => setOpen(false)}/>
            </Modal>
            <Tooltip title={"Create New Course"} placement={"top"}>
                <div className={"course-options-button clickable no-select"} onClick={ handleButtonClicked }>
                    <MdCreateNewFolder size={40}/>
                </div>
            </Tooltip>
        </>
    );
}


export default CourseOptionsButton;