import CourseOptionsButton from "./CourseOptionsButton";
import {useEffect, useState} from "react";
import useAxios from "../../../../hooks/useAxios";
import {Path, ServerPath} from "../../../../data/paths";
import toast from "react-hot-toast";
import CourseButton from "./CourseButton";
import useDashboardContext from "../../../../hooks/useDashboardContext";
import {Block, ControlBar, ControlItem, EmptyItem} from "../../../../components/navbar/ControlBar";
import {AiFillPlusSquare, AiOutlineAppstoreAdd, AiOutlineUnorderedList} from "react-icons/ai";
import Modal from "../../../../components/modal/Modal";
import {CourseInfoPopupEditable} from "./CourseInfoPopup";
import {RiUserShared2Line} from "react-icons/ri";
import {useCourses} from "../../../../hooks/useCourses";
import {useNavigate} from "react-router-dom";

const CourseDisplay = () => {

    const { courses, setCourses } = useDashboardContext();
    const [newCourseOpen, setNewCourseOpen] = useState(false);
    const navigate = useNavigate();

    const { getCourses } = useCourses();

    useEffect(() => {
        getCourses().then(c => setCourses(c)).catch((err) => err && toast.error(err));
    }, [])

    return (
        <section className={"display"}>
            <Modal preventAutoClose open={newCourseOpen} handleClose={() => setNewCourseOpen(false)}>
                <CourseInfoPopupEditable edit={false} onClose={() => setNewCourseOpen(false)}/>
            </Modal>
            <h1 className={"section-header cover-header"}>My Courses</h1>
            <ControlBar>
                <ControlItem toolLocation={"top"} tooltip={"Shared with Me"}><RiUserShared2Line size={20} /></ControlItem>
                <Block>
                    <ControlItem toolLocation={"top"} tooltip={"List all Courses"}><AiOutlineUnorderedList size={20} /></ControlItem>
                    <ControlItem onClick={() => navigate(Path.Dashboard.COURSE_CREATE)} toolLocation={"top"} tooltip={"Add Course"}><AiOutlineAppstoreAdd size={20}/></ControlItem>
                </Block>
         {/*<ControlItem toolLocation={"right"} tooltip={"Add Section"}><AiFillPlusSquare size={20}/></ControlItem>*/}
            </ControlBar>
            <div className={"course-container"}>
                {
                    courses &&
                    courses.map(c => <CourseButton key={c._id} course={c}/>)
                }

                {
                    courses?.length === 0 &&
                    <p className={"text-light"}>No Courses to Display</p>
                }

                {/*<CourseOptionsButton />*/}
            </div>
        </section>
    );
}

export default CourseDisplay;