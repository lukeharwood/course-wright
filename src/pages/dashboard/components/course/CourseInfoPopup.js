import {
    Chip,
    Divider,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    TextareaAutosize,
    TextField
} from "@mui/material";
import {useEffect, useRef, useState} from "react";
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import CreatableSelect from "react-select/creatable";
import {
    convertSelectObjectArrayToStringArray,
    convertStringArrayToSelectObjectArray
} from "../../../../utils/inputConversion";
import {CourseShareOptions, Defaults} from "../../../../data/enums";
import {shareOptionsTranslate} from "../../../../utils/messaging/courseOptionTranslator";
import toast from "react-hot-toast";
import {undefinedOrValue, validCourseCode} from "../../../../utils/validation";
import useDashboardContext from "../../../../hooks/useDashboardContext";
import ColorPicker from "../../../../components/input/ColorPicker";
import {useCourses} from "../../../../hooks/useCourses";

export const CourseInfoPopupEditable = ({ course, onClose, edit=false }) => {
    const [color, setColor] = useState(course?.color || Defaults.COURSE_COLOR);
    const [visibility, setVisibility] = useState(course?.visibility || Defaults.COURSE_VISIBILITY);
    const [name, setName] = useState(course?.name || "");
    const [shareOption, setShareOption] = useState(course?.shareOption || CourseShareOptions.restrict);
    const [code, setCode] = useState(course?.code || "");
    const [tags, setTags] = useState( course?.tags || []);
    const [subject, setSubject] = useState(course?.subject || "");
    const [description, setDescription] = useState(course?.description || "");
    // TODO: add collaborators
    const [collaborators, setCollaborators] = useState([]);
    const [nameError, setNameError] = useState(false);
    const [codeError, setCodeError] = useState(false);

    const submitRef = useRef();
    const { setCourses } = useDashboardContext();
    const { updateCourse } = useCourses();

    const validate = (name, code) => {
        let valid = true;
        const formattedCode = validCourseCode(code);
        if (!formattedCode) {
            setCodeError(true);
            valid = false;
        } else {
            setCodeError(false);
            setCode(formattedCode);
        }
        if (name.trim() === "") {
            setNameError(true);
            valid = false;
        } else {
            setNameError(false);
        }
        return valid;
    }

    const submit = async (e) => {
        if (!validate(name, code)) {
            return;
        }
        const id = toast.loading(`Saving ${course.name}...`);
        updateCourse(course._id, {
            color, name, code, tags, subject, description, visibility, shareOption
        })
            .then(r => {
                setCourses(prev => {
                    return prev.map((c) => {
                        if (c._id === r._id) {
                            return r;
                        } else {
                            return c;
                        }
                    })
                })
                onClose();
                toast.success("Course Saved.", { id });
            })
            .catch(err => {
                if (err) toast.error(err);
            });
    }

    const onEnter = (e) => {
        if (e.key === "Enter") {
            if (validate(name, code)) {
                submitRef.current.scrollIntoView();
            }
        }
    }

    return (
        <>
            <div onKeyUp={ onEnter } className={"course-info-popup"}>
                <Divider variant={"middle"} textAlign={"center"}><Chip label={"Course Info"}/></Divider>
                <section className={"course-name-container"}>
                    <TextField value={ name } onChange={(e) => setName(e.target.value)} error={nameError} helperText={"Required"} required sx={{ width: "60%"}} id="course-name" label="Course Name" variant="outlined" />
                    <TextField value={ code } onChange={(e) => setCode(e.target.value)} error={codeError} helperText={"Example: CS-101"} required sx={{ width: "40%"}} id="course-code" label="Course Code" variant="outlined" />
                </section>
                <Divider variant={"middle"} textAlign={"center"}><Chip label={"Meta Data"}/></Divider>
                <section className={"section-spacing"}>
                    <div className={"flex justify-between align-center"}>
                        <TextField style={{ margin: "1rem 0"}} value={ subject } onChange={(e) => setSubject(e.target.value.toLowerCase())} sx={{ width: "60%"}} id="course-subject" label="Course Subject" variant="standard" />
                        <ColorPicker label={"Course Color"} size={40} color={ color } onChange={(e) => setColor(e.target.value)} />
                    </div>
                    <CreatableSelect value={ convertStringArrayToSelectObjectArray(tags) } onChange={(newValue) => setTags(convertSelectObjectArrayToStringArray(newValue))} formatCreateLabel={(value) => `Add "${value.toLowerCase()}" tag`} placeholder={"Tags"} isMulti/>
                </section>
                <Divider variant={"middle"} textAlign={"center"}><Chip label={"Accessibility"}/></Divider>
                <section className={"accessibility"}>
                    <div>
                        <FormControl sx={{mt: 2, minWidth: 120, maxWidth: 120 }}>
                            <InputLabel id={"visibility-label"}>Visibility</InputLabel>

                            <Select
                                labelId={"visibility-label"}
                                id={"visibility"}
                                label={"Visibility"}
                                value={visibility}
                                onChange={(e) => setVisibility(e.target.value)}
                            >
                                <MenuItem value={"public"}>Public</MenuItem>
                                <MenuItem value={"private"}>Private</MenuItem>
                            </Select>
                        </FormControl>
                        <FormHelperText>{
                            visibility === "private" ? "Project is visible to only you." : "Project can be found by our search feature."
                        }</FormHelperText>
                    </div>
                </section>
                <Divider variant={"middle"} textAlign={"center"}><Chip label={"Collaborators"}/></Divider>
                <section id={"collaborators-class"} className={"section-spacing"}>
                    <FormControl sx={{minWidth: 200, maxWidth: 120 }}>
                        <InputLabel id={"sharing-label"}>Sharing</InputLabel>

                        <Select
                            labelId={"sharing-label"}
                            id={"sharing"}
                            label={"Sharing"}
                            value={ shareOption }
                            onChange={(e) => setShareOption(e.target.value)}
                        >
                            <MenuItem value={CourseShareOptions.restrict}>Restrict Access</MenuItem>
                            <MenuItem value={CourseShareOptions.link}>Anyone With Link</MenuItem>
                            <MenuItem value={CourseShareOptions.invite}>Add By Email</MenuItem>
                        </Select>
                    </FormControl>
                    <FormHelperText>{ shareOptionsTranslate(shareOption) }</FormHelperText>
                    <CreatableSelect isDisabled={ shareOption === CourseShareOptions.restrict } formatCreateLabel={(value) => `No user found.`} placeholder={"Collaborator Email"} isMulti/>
                </section>
                <div ref={submitRef} className={"bottom-options-panel-course-info-popup"}>
                    <button onClick={ onClose } className={"btn btn-danger options-button"}>Cancel</button>
                    <button onClick={ submit } className={"btn btn-primary options-button"}>{ edit ? "Save" : "Create"}</button>
                </div>
            </div>
        </>
    )
}
