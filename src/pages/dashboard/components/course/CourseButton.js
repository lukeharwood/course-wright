import { VscSettings } from 'react-icons/vsc'
import {parseCourseCode} from "../../../../utils/regex";
import toast from "react-hot-toast";
import {useState} from "react";
import Modal from "../../../../components/modal/Modal";
import {CourseInfoPopupEditable} from "./CourseInfoPopup";
import {Tooltip} from "@mui/material";
import useDashboardContext from "../../../../hooks/useDashboardContext";
import {useNavigate} from "react-router-dom";
import {Path} from "../../../../data/paths";

const CourseButton = ({ course }) => {

    const { code, name, _id, access, subject, color } = course;
    // modal window for course options
    const [open, setOpen] = useState(false);
    const { sections } = useDashboardContext();
    const navigate = useNavigate();


    const handleSectionsClicked = (e) => {
        // TODO: feature has been moved to a future release
        // e.stopPropagation();
        // toast.success("Displaying Sections...");
    }

    const handleButtonClicked = (e) => {
        e.stopPropagation();
        navigate(Path.Dashboard.COURSE_VIEW(_id))
    }

    const handleMoreButtonClicked = (e) => {
        e.stopPropagation();
        setOpen(true);
    }

    const getNumSections = () => {
        return sections.filter(s => s.course === _id).length;
    }

    return (
        <div className={"course-button clickable no-select"} onClick={ handleButtonClicked }>
            <Modal open={open} handleClose={() => setOpen(false)} preventAutoClose={false}>
                <CourseInfoPopupEditable onClose={() => setOpen(false) } course={course} edit={ true }/>
            </Modal>
            <div className={"code"} style={{ backgroundColor: color }}>
                <h1>{ parseCourseCode(name, code) }</h1>
            </div>
            <div className={"course-button-middle-container"}>
                <Tooltip placement={"top"} title={name}>
                    <p className={"name"}>{name}</p>
                </Tooltip>
                <p className={"members"}>{access.length} {access.length === 1 ? "member" : "members"}</p>
                {
                    sections && <p onClick={ handleSectionsClicked } className={"members"}>{ getNumSections() } active {getNumSections() === 1 ? "section" : "sections"}</p>
                }
            </div>
            <Tooltip placement={"top"} title={"Course Settings"}>
                <button onClick={ handleMoreButtonClicked } className={"course-button-more-button"}><VscSettings className={"text-light"} size={20} /></button>
            </Tooltip>
        </div>
    );
}

export default CourseButton;