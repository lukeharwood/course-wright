import {Divider, Tooltip} from "@mui/material";
import {MdOutlineAppRegistration, MdOutlinePendingActions} from "react-icons/md";
import {StatusTag, Tag} from "../../../../components/labeling/Tags";
import {SectionStatusToActiveStatus, ActiveStatus} from "../../../../data/enums";
import {useEffect, useState} from "react";
import {useCourses} from "../../../../hooks/useCourses";
import toast from "react-hot-toast";

const ClassPreview = ({ section }) => {

    const [course, setCourse] = useState({});
    const { getCourse } = useCourses();

    useEffect(() => {
        getCourse(section.course).then(c => setCourse(c)).catch(err => err && toast.error(err));
    }, [section])

    return (
        <div className={"section-preview-card no-select"}>
            <div className={"card-top"}>
                <div>
                    <Tooltip placement={"top"} title={course?.name}>
                        <h1 className={"section-card-name"}>{ course?.name }  <StatusTag status={SectionStatusToActiveStatus[section.status]} text={section.status}/></h1>
                    </Tooltip>
                    <Tag color={"gray"} text={course?.code} className={"section-code"}/>
                    <Tag color={section.color} text={section.number} className={"section-code section-number-code"} />
                </div>
            </div>

            <div className={"card-bottom"}>
                <div className={"left-button-group icon-group clickable"}>
                    <MdOutlinePendingActions className={"icon-group-icon text-medium"} size={20}/>
                    <p className={"card-button-text"}>Open</p>
                </div>
                <div className={"right-button-group icon-group clickable"}>
                    <MdOutlineAppRegistration className={"icon-group-icon text-medium"} size={20} />
                    <p className={"card-button-text"}>Grade</p>
                </div>
            </div>
        </div>
    );
}

export default ClassPreview;