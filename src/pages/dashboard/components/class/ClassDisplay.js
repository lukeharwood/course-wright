import ClassPreview from "./ClassPreview";
import CourseOptionsButton from "../course/CourseOptionsButton";
import {Block, ControlBar, ControlItem, EmptyItem, ToggleControlItem} from "../../../../components/navbar/ControlBar";
import {AiFillPlusSquare, AiOutlineUnorderedList} from "react-icons/ai";
import useDashboardContext from "../../../../hooks/useDashboardContext";
import {useEffect, useState} from "react";
import useAxios from "../../../../hooks/useAxios";
import toast from "react-hot-toast";
import {Path, ServerPath} from "../../../../data/paths";
import {useNavigate} from "react-router-dom";
import {useSections} from "../../../../hooks/useSections";
import {AiOutlineHistory} from "react-icons/ai";
import {SectionStatus} from "../../../../data/enums";

const ClassDisplay = () => {

    const { sections } = useDashboardContext();
    const navigate = useNavigate();
    const [past, setPast] = useState(false);
    const [current, setCurrent] = useState(true);
    const [filteredSections, setFilteredSections] = useState([]);

    useEffect(() => {
        setFilteredSections(sections.filter(s => {
            return (((s.status === SectionStatus.OPEN || s.status === SectionStatus.IN_PROGRESS) && current) ||
                ((s.status === SectionStatus.CLOSED || s.status === SectionStatus.ENDED) && past))
        }));
    }, [past, current, sections])


    const handleCreateSection = () => {
        navigate(Path.Dashboard.CLASS_CREATE);
    }

    return (
        <section className={"section-display display"}>
            <h1 className={"section-header"}>My Classes</h1>

            <ControlBar align={"right"}>
                <Block key={"block"}>
                    <ToggleControlItem selected={past} key={0} onClick={() => setPast((p) => !p)} tooltip={"Inactive Sections"}><AiOutlineHistory size={20} /></ToggleControlItem>
                    <ToggleControlItem selected={current} onClick={() => setCurrent((p) => !p)} key={1} tooltip={"Active Sections"}><AiOutlineUnorderedList size={20} /></ToggleControlItem>
                    <ControlItem key={2} onClick={ handleCreateSection } tooltip={"Create Section"}><AiFillPlusSquare size={20}/></ControlItem>
                </Block>
           </ControlBar>
            <div className={"section-container"}>
                {
                    sections?.length > 0 && filteredSections.map((s, i) => <ClassPreview key={i} section={s} />)
                }
                {
                    filteredSections?.length === 0 && <p key={"55"} className={"text-light section-none-text"}>There are no classes to display.</p>
                }
            </div>
        </section>
    );
}

export default ClassDisplay;