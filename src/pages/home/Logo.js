import { motion } from 'framer-motion';
import './home.css';

export const LogoText = () => {
    return (
        <div className={"logo-text no-select"}>
            <h1 className={"nowrap"}><span className={"gradient-text"}>Course</span><span className={"wright-text"}>W<span className={"underline"}>right.</span></span></h1>
        </div>
    );
}

const Logo = () => {
    return (
        <motion.div
            initial={{ rotate: -10, scale: 1.3 }}
            animate={{ rotate: 0, scale: 1 }}
            className={"logo no-select"}>
            <h1 className={"nowrap"}><motion.span
                initial={{ x: "100px" }}
                animate={{ x: 0 }}
                className={"logo-c"}>C</motion.span>
                <motion.span
                    initial={{ y: "100px" }}
                    animate={{ y: 0 }}
                    className={"logo-w"}>W</motion.span></h1>
        </motion.div>
    );
}



export default Logo;