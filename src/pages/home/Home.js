import NavBar from "../../components/navbar/NavBar";
import Logo, { LogoText } from "./Logo";
import './home.css';
import {AiOutlineBuild} from "react-icons/ai";
import { FaHandsHelping } from 'react-icons/fa'
import { BiWorld } from 'react-icons/bi'

const Home = () => {
    return (
          <div>
                <div className={"hero section"}>
                    <Logo />
                    <LogoText />
                </div>

              <div className={"card-layout"}>
                  <div className={"card"}>
                      <AiOutlineBuild size={ 30 }/>
                      <h2>Build Material with Ease.</h2>
                      <p>
                          Create collections of homework items, educational readings, etc...
                      </p>
                  </div>

                  <div className={"card"}>
                      <FaHandsHelping size={ 30 } />
                      <h2>Collaborate with Other Creators.</h2>
                      <p>
                          Create collections of homework items, educational readings, etc...
                      </p>
                  </div>
                  <div className={"card"}>
                      <BiWorld size={ 30 } />
                      <h2>Share with the World.</h2>
                      <p>
                          Create collections of homework items, educational readings, etc...
                      </p>
                  </div>
              </div>

          </div>
    );
}

export default Home;