import Logo from "../home/Logo";
import './404.css'
const NotFound = () => {
    return (
        <div className={"flex justify-center gap-5 align-center"}>
            <Logo />
            <div className={"sm-container"}>
                <h1>404 Not Found</h1>
                <p className={"mono sm-container"}>Sorry, we are unable to find the page you're looking for...</p>
            </div>
        </div>
    );
}

export default NotFound;