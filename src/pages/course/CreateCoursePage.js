import {useRef, useState} from "react";
import "./course.css"
import {CourseShareOptions, Defaults} from "../../data/enums";
import useAxios from "../../hooks/useAxios";
import useDashboardContext from "../../hooks/useDashboardContext";
import {undefinedOrValue, validCourseCode} from "../../utils/validation";
import toast from "react-hot-toast";
import {Path, ServerPath} from "../../data/paths";
import {Chip, Divider, FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import ColorPicker from "../../components/input/ColorPicker";
import CreatableSelect from "react-select/creatable";
import { motion } from 'framer-motion'
import {
    convertSelectObjectArrayToStringArray,
    convertStringArrayToSelectObjectArray
} from "../../utils/inputConversion";
import {shareOptionsTranslate} from "../../utils/messaging/courseOptionTranslator";
import {useNavigate} from "react-router-dom";
import Dashboard from "../dashboard/Dashboard";
import ConfirmModal from "../../components/modal/ConfirmModal";
import {Tag} from "../../components/labeling/Tags";
import SectionHeader, {SectionSubHeader} from "../../components/labeling/SectionHeader";

const CreateCoursePage = () => {
    const [color, setColor] = useState(Defaults.COURSE_COLOR);
    const [visibility, setVisibility] = useState( Defaults.COURSE_VISIBILITY);
    const [name, setName] = useState("");
    const [shareOption, setShareOption] = useState(CourseShareOptions.restrict);
    const [code, setCode] = useState("");
    const [tags, setTags] = useState( []);
    const [subject, setSubject] = useState( "");
    const [description, setDescription] = useState("");
    // TODO: add collaborators
    const [collaborators, setCollaborators] = useState([]);
    const [nameError, setNameError] = useState(false);
    const [codeError, setCodeError] = useState(false);

    const [confirmModalOpen, setConfirmModalOpen] = useState(false);
    const navigate = useNavigate();

    const submitRef = useRef();

    const axios = useAxios();

    const { setCourses } = useDashboardContext();

    const validate = (name, code) => {
        let valid = true;
        const formattedCode = validCourseCode(code);
        if (!formattedCode) {
            setCodeError(true);
            valid = false;
        } else {
            setCodeError(false);
            setCode(formattedCode);
        }
        if (name.trim() === "") {
            setNameError(true);
            valid = false;
        } else {
            setNameError(false);
        }
        return valid;
    }

    const submitNew = async () => {
        const id = toast.loading("Creating Course...");
        const data = {
            color,
            name,
            code: undefinedOrValue(code),
            tags,
            subject: undefinedOrValue(subject),
            description: undefinedOrValue(description),
            visibility,
            shareOption
        }
        try {
            const response = await axios.post(ServerPath.Dashboard.CREATE_EDIT_COURSE(), data);
            if (response.status === 500) {
                toast.error("Sorry, there was an issue... try again.");
            } else {
                const { course } = response.data;
                if (course) {
                    toast.success("Course Added.", { id });
                    navigate(Path.DASHBOARD);
                } else {
                    toast.error("Something went wrong. Please try again.", { id });
                }
            }
        } catch (err) {
            console.log(err);
            toast.remove(id);
        }
    }

    const submit = async (e) => {
        if (!validate(name, code)) {
            return;
        }
        await submitNew();
    }

    const onEnter = (e) => {
        if (e.key === "Enter") {
            if (validate(name, code)) {
                submitRef.current.scrollIntoView();
            }
        }
    }

    const handleCancel = () => {
        navigate(Path.DASHBOARD);
    }

    return (
        <div>
            <div style={{ boxShadow: `${color} 5px 5px 0 0`}} className={"new-course-header"}>
                <h1>{ name.trim() === "" ? "New Course" : name }</h1>
                <Tag color={color} text={validCourseCode(code) ? code : "New"} />
                <hr className={"text-white"}/>
            </div>
            <div className={"create-course-body"}>
                <ConfirmModal open={confirmModalOpen} handleClose={() => setConfirmModalOpen(false)}
                              preventAutoClose={true} submessage={"All progress will be lost"} message={"Are you sure?"} onConfirm={() => handleCancel()} onReject={() => setConfirmModalOpen(false)} confirmText={"Yes"} rejectText={"No"}/>
                <div onKeyUp={ onEnter } className={"create-course-page"}>
                    <div className={"create-course-section"}>
                        <SectionHeader text={"Course Info"} color={"white"} bg={color} />
                        <section className={"course-name-container"}>
                            <TextField value={ name } onChange={(e) => setName(e.target.value)} error={nameError} helperText={"Required"} required sx={{ width: "60%"}} id="course-name" label="Course Name" variant="outlined" />
                            <TextField value={ code } onChange={(e) => setCode(e.target.value)} error={codeError} helperText={"Example: CS-101"} required sx={{ width: "40%"}} id="course-code" label="Course Code" variant="outlined" />
                        </section>
                        <SectionSubHeader bg={ color } text={"Meta Data"} color={"white"} />
                        <section className={"section-spacing"}>
                            <div className={"flex justify-between align-center"}>
                                <TextField style={{ margin: "1rem 0"}} value={ subject } onChange={(e) => setSubject(e.target.value.toLowerCase())} sx={{ width: "60%"}} id="course-subject" label="Course Subject" variant="standard" />
                                <ColorPicker label={"Course Color"} size={40} color={ color } onChange={(e) => setColor(e.target.value)} />
                            </div>
                            <CreatableSelect value={ convertStringArrayToSelectObjectArray(tags) } onChange={(newValue) => setTags(convertSelectObjectArrayToStringArray(newValue))} formatCreateLabel={(value) => `Add "${value.toLowerCase()}" tag`} placeholder={"Tags"} isMulti/>
                        </section>
                    </div>
                    <div className={"create-course-section"}>
                        <SectionSubHeader bg={ color } text={"Accessibility"} color={"white"} />
                        <section className={"accessibility"}>
                            <div>
                                <FormControl sx={{mt: 2, minWidth: 120, maxWidth: 120 }}>
                                    <InputLabel id={"visibility-label"}>Visibility</InputLabel>

                                    <Select
                                        labelId={"visibility-label"}
                                        id={"visibility"}
                                        label={"Visibility"}
                                        value={visibility}
                                        onChange={(e) => setVisibility(e.target.value)}
                                    >
                                        <MenuItem value={"public"}>Public</MenuItem>
                                        <MenuItem value={"private"}>Private</MenuItem>
                                    </Select>
                                </FormControl>
                                <FormHelperText>{
                                    visibility === "private" ? "Project is visible to only you." : "Project can be found by our search feature."
                                }</FormHelperText>
                            </div>
                        </section>
                        <SectionSubHeader bg={ color } text={"Collaborators"} color={"white"} />
                        <section id={"collaborators-class"} className={"section-spacing"}>
                            <FormControl sx={{minWidth: 200, maxWidth: 120 }}>
                                <InputLabel id={"sharing-label"}>Sharing</InputLabel>

                                <Select
                                    labelId={"sharing-label"}
                                    id={"sharing"}
                                    label={"Sharing"}
                                    value={ shareOption }
                                    onChange={(e) => setShareOption(e.target.value)}
                                >
                                    <MenuItem value={CourseShareOptions.restrict}>Restrict Access</MenuItem>
                                    <MenuItem value={CourseShareOptions.link}>Anyone With Link</MenuItem>
                                    <MenuItem value={CourseShareOptions.invite}>Add By Email</MenuItem>
                                </Select>
                            </FormControl>
                            <FormHelperText>{ shareOptionsTranslate(shareOption) }</FormHelperText>
                            <CreatableSelect isDisabled={ shareOption === CourseShareOptions.restrict } formatCreateLabel={(value) => `No user found.`} placeholder={"Collaborator Email"} isMulti/>
                        </section>
                        <div ref={submitRef} className={"bottom-options-panel-course-info-popup"}>
                            <button onClick={ () => setConfirmModalOpen(true) } className={"btn btn-danger options-button"}>Cancel</button>
                            <button onClick={ submit } className={"btn btn-primary options-button"}>{"Create"}</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default CreateCoursePage;