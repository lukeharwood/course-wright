
export const CourseShareOptions = {
    restrict: "block-access",
    link: "anyone-with-link",
    invite: "invite-by-email",
}

export const Defaults = {
    COURSE_COLOR: "purple",
    COURSE_VISIBILITY: "private",
    SECTION_NUMBERS: ["001", "002", "011", "012", "021", "022"],
    SECTION_COLORS: ["#7CB9E8", "#9F2B68", "#3B7A57", "#9966CC", "#FBCEB1", "#A1CAF1"]
}

export const ActiveStatus = {
    ACTIVE: "active",
    INACTIVE: "inactive",
    DANGER: "danger",
}

export const SectionStatusToActiveStatus = {
    "in-progress": ActiveStatus.ACTIVE,
    "closed": ActiveStatus.INACTIVE,
    "ended": ActiveStatus.DANGER,
    "open": ActiveStatus.ACTIVE
};

export const SectionStatus = {
    IN_PROGRESS: "in-progress",
    CLOSED: "closed",
    OPEN: "open",
    ENDED: "ended",
};