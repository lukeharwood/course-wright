export const ServerPath = {
    SIGN_IN_URL: "/auth",
    Dashboard: {
        CREATE_EDIT_COURSE: (id) => id ? `/course/${id}` : "/course",
        COURSE_BY_ID: (id) => `/course/${id}`,
        GET_COURSES: "/courses",
        COURSE: "/course",
        SECTIONS: "/sections",
    }
}

export const Path = {
    Dashboard: {
        CLASS_CREATE: "/class/create",
        CLASS_CREATE_ID: (id) => `/class/create/${id}`,
        COURSE_CREATE: "/course/create",
        COURSE_VIEW: (id) => `/course/${id}`,
    },
    DASHBOARD: "/dashboard",
    DASHBOARD_SECTIONS: "/dashboard#sections"
}