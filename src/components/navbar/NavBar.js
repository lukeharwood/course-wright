import { GiHamburgerMenu } from 'react-icons/gi'
import {Drawer} from "@mui/material";
import DrawerContents from "./DrawerContents";
import {useState} from "react";
import { motion } from 'framer-motion';
import {TagLink} from "../labeling/Tags";
import {Path} from "../../data/paths";
import {useLocation} from "react-router-dom";
import {AiOutlineHome} from "react-icons/ai";
import useAuth from "../../hooks/useAuth";

const NavBar = () => {
    const [open, setOpen] = useState(false);
    const { auth } = useAuth();
    const location = useLocation();
    return (
        <nav className={"nav-bar no-select"}>

            <motion.a
                whileHover={{ scale: 1.2, boxShadow: "rgb(100, 100, 100) 10px 10px 0 -7px"}}
                href={"/"}
                className={"logo-container"}>
                <h1 className={"nowrap"}><span className={"logo-c"}>C</span>W</h1>
            </motion.a>

            {
                location.pathname !== Path.DASHBOARD && auth?.user && <TagLink icon={<AiOutlineHome size={20} />} href={Path.DASHBOARD} text={`My Dashboard`} color={"var(--theme-blue)"}/>
            }

            <div>
                <button onClick={() => setOpen(true)} className={"btn btn-transparent"}><GiHamburgerMenu size={ 30 }/></button>
            </div>

            <Drawer
                anchor={'right'}
                open={open}
                onClose={ () => setOpen(false) }
            >
                <DrawerContents onClose={ () => setOpen(false) }/>
            </Drawer>
        </nav>
    );
}

export default NavBar;