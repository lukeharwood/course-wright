import { motion } from "framer-motion";
import {useNavigate} from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import useLogout from "../../hooks/useLogout";

const DrawerContents = ({ onClose }) => {

    const navigate = useNavigate()
    const { auth } = useAuth();
    const logout = useLogout();

    const handleNavigate = (url) => {
        navigate(url);
        onClose();
    }

    return (
        <section className={"drawer-contents no-select"}>
            <ul className={"nav-items"}>
                <motion.li whileHover={{
                    scale: 1.1
                }} onClick={  () => handleNavigate("/") }  className={"hover-link"}>Search</motion.li>
                <motion.li whileHover={{
                    scale: 1.1
                }} onClick={  () => handleNavigate("/dashboard") }  className={"hover-link"}>Dashboard</motion.li>
                {
                    auth?.user && <motion.li whileHover={{
                        scale: 1.1
                    }} onClick={  () => {
                        navigate("/profile")
                        onClose()
                    }}  className={"hover-link"}>My Profile</motion.li>
                }
                {
                    auth.user ? <motion.li whileHover={{
                        scale: 1.1
                    }} onClick={  () => {
                        logout();
                        onClose()
                    }}  className={"hover-link"}>Sign Out</motion.li> : <motion.li whileHover={{
                        scale: 1.1
                    }} onClick={  () => handleNavigate("/sign-in") }  className={"hover-link"}>Sign In</motion.li>
                }
            </ul>
        </section>
    )
}

export default DrawerContents;