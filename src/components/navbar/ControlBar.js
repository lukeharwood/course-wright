import './nav.css'
import {Tooltip} from "@mui/material";

export const ControlBar = ({wrapperStyle, children, align="none" }) => {
    return (
        <div style={wrapperStyle} className={"control-bar"}>
            {
                (align === "right" || align === "center") && <EmptyItem key={"empty1"} />
            }
            { children }
            {
                (align === "left" || align === "center") && <EmptyItem key={"empty2"}/>
            }
        </div>
    );
}

export const Block = ({ children }) => {
    return (
        <div className={"control-block"}>
            { children }
        </div>
    );
}

export const ControlItem = ({ disabled=false, toolLocation="top", tooltip, onClick, children }) => {
    return (
        <Tooltip placement={toolLocation} title={tooltip}>
            <div onClick={onClick} className={`control-item ${disabled ? "control-item-disabled" : "control-item-enabled"}`}>
                { children }
            </div>
        </Tooltip>
    );
}

export const ToggleControlItem = ({ selected=false, toolLocation="top", tooltip, onClick, children }) => {
    return (
        <Tooltip placement={toolLocation} title={tooltip}>
            <div onClick={onClick} className={`control-item-toggle ${selected ? "control-item-selected": ""} clickable no-select`}>
                { children }
            </div>
        </Tooltip>
    );
}

/**
 * Control Bar has property `justify-content: space-between;`
 * This element is for offsetting a block or single `ControlItem`
 * @returns {JSX.Element}
 * @constructor
 */
export const EmptyItem = () => {
    return <div></div>;
}