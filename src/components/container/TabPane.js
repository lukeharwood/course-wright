import {useState} from "react";
import {Block, ControlBar, EmptyItem, ToggleControlItem} from "../navbar/ControlBar";
import './containers.css'
const TabPane = ({ className="", containerStyle, children, width, nextButton=false, tabLabels, tabHeader=false, numbered=false, allowPrevious, allowNext, onError, allowSkip=false }) => {
    const [index, setIndex] = useState(0);

    const handleMove = (i) => {
        if (i === index + 1) {
            if (allowNext) {
                setIndex(i)
            }
        } else if (i === index - 1) {
            if (allowPrevious) {
                setIndex(i)
            }
        } else if (allowSkip) {
            setIndex(i);
        } else {
            onError && onError();
        }
    }

    return (
        <div style={ containerStyle } className={`tab-pane ${className}`}>
            <ControlBar>
                {
                    numbered && tabLabels && tabHeader ? <h1 className={"tab-header no-select"}>{ tabLabels[index] }</h1> : <EmptyItem />
                }
                <Block>
                    {
                        children?.length && children.map((c, i) => {
                            return <ToggleControlItem key={i} onClick={() => handleMove(i)} selected={i === index} toolLocation={"top"} tooltip={tabLabels[i]}>{ numbered ? i + 1 : tabLabels[i]}</ToggleControlItem>
                        })
                    }
                </Block>
            </ControlBar>
            { children?.length ? children[index] : children }
            {
                nextButton && children?.length && children?.length !== index + 1 &&
                <ControlBar wrapperStyle={{ marginTop: "5px"}} align={"right"}> <button onClick={() => setIndex((prevState) => prevState+1)} className={"next-button"} disabled={!allowNext}>Next</button></ControlBar>
            }
        </div>
    )
}

export default TabPane;