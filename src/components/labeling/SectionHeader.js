import './mini.css'

const SectionHeader = ({ bg, color="black", text }) => {
    return (
        <h1 className={"section-header-label"} style={{ backgroundColor: bg, color }}>{ text }</h1>
    )
}

export const SectionSubHeader = ({ align="center", bg, color="black", text }) => {
    return (
        <h3 className={"section-subheader-label"} style={{ textAlign: align, backgroundColor: bg, color }}>{ text }</h3>
    )
}

export default SectionHeader;