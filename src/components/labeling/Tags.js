import './mini.css'
import {ActiveStatus} from "../../data/enums";
import toast from "react-hot-toast";
import {useNavigate} from "react-router-dom";
import { motion } from 'framer-motion';

export const StatusTag = ({status, text}) => {
    const classifier = (status === ActiveStatus.ACTIVE) ? "success-theme" : (status === ActiveStatus.INACTIVE) ? "warning-theme" : "danger-theme";
    return <span className={`tag ${classifier}`}>{text}</span>
}

export const Tag = ({ margin, text, color, className }) => {
    return <span style={{ margin, backgroundColor: color }} className={`tag ${className} shadow text-white`}>{text}</span>
}

export const TagButton = ({ margin, padding, text, color, className }) => {
    return <div style={{ padding, margin, backgroundColor: color }} className={"tag-button shadow text-white"}>{text}</div>
}

export const TagLink = ({ icon, margin, padding, text, color, href, theme=false }) => {
    const navigate = useNavigate();
    return <motion.div
        whileHover={{ scale: 1.1 }} onClick={() => navigate(href)} style={{ padding,
        margin,
        backgroundColor: color }} className={`tag-button ${theme ? "tag-button-theme" : ""} shadow text-white`}>{icon}{text}</motion.div>
}