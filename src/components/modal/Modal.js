import './modal.css';
import {AnimatePresence, motion, transform} from 'framer-motion';

const Modal = ({ open, handleClose, children, preventAutoClose=false }) => {
    const handleAutoClose = (e) => {
        e.stopPropagation();
        if (!preventAutoClose) {
            handleClose();
        }
    }
    return (
       <AnimatePresence>
           {
               open &&
               <motion.div
                   key={"modal-window"}
                   initial={{ opacity: 0 }}
                   animate={{ opacity: 1 }}
                   exit={{ opacity: 0 }}
                   onClick={handleAutoClose} className={"window"}>
                   <motion.div
                       initial={{ scale: 0 }} animate={{ y: 0, scale: 1 }} exit={{ scale: 0 }}
                       onClick={(e) => e.stopPropagation()} className={"modal"}>
                       { children }
                   </motion.div>
               </motion.div>
           }
       </AnimatePresence>

   )
}

export default Modal;