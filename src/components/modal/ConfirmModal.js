import Modal from "./Modal";
import './modal.css';

const ConfirmModal = ({ confirmText="Ok", rejectText="Cancel", message, submessage, open, handleClose, onConfirm, onReject, preventAutoClose=true}) => {
    const m = message || "Are you sure?";
    const handleConfirm = () => {
        if (onConfirm) onConfirm();
        handleClose();
    }

    const handleReject = () => {
        if (onReject) onReject();
        handleClose();
    }
    return (
        <Modal handleClose={ handleClose } open={ open } preventAutoClose={ true }>
            <div className={"confirm-modal"}>
                <div>
                    <h1 className={"confirm-text"}>{m}</h1>
                    {
                        submessage && <p className={"submessage"}>{ submessage } </p>
                    }
                </div>
                <div className={"confirm-button-group"}>
                    <button onClick={ handleReject } className={"options-button btn btn-danger"}>{ rejectText }</button>
                    <button onClick={ handleConfirm } className={"options-button btn btn-primary"}>{ confirmText }</button>
                </div>
            </div>
        </Modal>
    );
}

export default ConfirmModal;