import './input.css';
import {useEffect, useRef, useState} from "react";
import { AiOutlineCheck } from 'react-icons/ai';


export const EditableText = ({textClassName, value, onValueChange, onRegexFail, regex=/.*/}) => {
    const [edit, setEdit] = useState(false);
    const [v, setV] = useState(value);

    const submit = (onFail) => {
        const res = regex.exec(v);
        if (res && (res[0] === v)) {
            onValueChange(v);
            setEdit(false);
            setV(value);
        } else {
            onRegexFail && onRegexFail();
            onFail()
        }
    }

    useEffect(() => {
        setV(value);
    }, [value])

    const handleBlur = () => {
        setV(value);
        setEdit(false);
    }

    const handleSubmitListener = (e) => {
        if (e.key === "Enter") {
            submit();
        }
    }

    const handleEnterEditMode = () => {
        setEdit(true);
    }

    return (
        <div>
            {
                edit ?
                    <SubmitTextElement onSubmit={submit} onKeyDown={handleSubmitListener} onBlur={handleBlur} value={v} onChange={(e) => setV(e.target.value)}/>
                    :
                    <p onDoubleClick={ handleEnterEditMode } className={textClassName}>{value}</p>
            }
        </div>
    );
}

const SubmitTextElement = ({ onKeyDown, onBlur, value, onChange, onSubmit }) => {
    const ref = useRef();
    useEffect(() => {
        ref.current.focus();
    }, [])

    const focus = () => {
        ref.current.focus()
    }

    return (
        <div className={"flex"}>
            <input className={"submit-text-element-input"} id={"text-input"} ref={ref} onKeyDown={onKeyDown} onBlur={onBlur} value={ value } onChange={ onChange}/>
            <button onSubmit={() => onSubmit(focus)} className={"submit-text-element-btn"}><AiOutlineCheck size={20}/></button>
        </div>
    );
}