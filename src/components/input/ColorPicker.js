import './input.css'
import {useRef} from "react";

const ColorPicker = ({ size, label, color, onChange }) => {
    const inputRef = useRef();

    const handleClick = () => {
        inputRef.current.click();
    }
    return (
        <div className={"color-picker"}>
            {
                label && <p className={"color-label"}>{ label }</p>
            }
            <div onClick={ handleClick } style={{ width: size, height: size, backgroundColor: color }} className={"color-picker-icon"}></div>
            <input onChange={ onChange } ref={inputRef} style={{ display: "none"}} type={"color"} />
        </div>
    );
}

export default ColorPicker;