import toast from "react-hot-toast";
import {CourseShareOptions} from "../../data/enums";

export const shareOptionsTranslate = (id) => {
    switch (id) {
        case CourseShareOptions.restrict:
            return "Only you can view the course."
        case CourseShareOptions.link:
            return "Anyone with this link can access the course materials."
        case CourseShareOptions.invite:
            return "Course is available to all with an invite."
        default:
            toast.loading(`Invalid Dev Option... ${id}`);
    }
}

export const autoEntranceTranslate = (id) => {
    if (id) {
        return "Students can enter class without approval.";
    } else {
        return "Students must be invited or accepted before entering class.";
    }
}

export const linkSharingTranslate = (id) => {
    if (id) {
        return "Section can be joined with a link.";
    } else {
        return "Link sharing is disabled.";
    }
}