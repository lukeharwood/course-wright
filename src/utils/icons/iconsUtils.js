import {AiOutlineLink, AiFillFile, AiOutlineFileMarkdown, AiOutlineFileWord, AiOutlineFileText, AiOutlineFilePdf, AiOutlineFileJpg, AiOutlineFileImage, AiOutlineFileExcel, AiOutlineFileZip, AiOutlineFileUnknown} from "react-icons/ai";
import toast from "react-hot-toast";
import {parseFileType} from "../regex";

export const getIconFromResourceType = (resource) => {
    if (resource.type === "file") {
        return getIconFromFileType(resource.name);
    } else if (resource.type === "page") {
        return <AiFillFile size={30}/>
    } else if (resource.type === "link") {
        return <AiOutlineLink size={30}/>
    } else {
        toast.loading("Unknown resource type...");
    }
}

const getIconFromFileType = (filename) => {
    let filetype = parseFileType(filename);
    if (filetype) filetype = filetype[1].toLowerCase();
    switch (filetype) {
        case "txt":
            return <AiOutlineFileText size={30} />
        case "png":
        case "jpg":
        case "jpeg":
            return <AiOutlineFileImage size={30} />
        case "pdf":
            return <AiOutlineFilePdf size={30} />
        case "zip":
            return <AiOutlineFileZip size={30} />
        case "md":
            return <AiOutlineFileMarkdown size={30} />
        default:
            return <AiOutlineFileUnknown size={30} />
    }
}
