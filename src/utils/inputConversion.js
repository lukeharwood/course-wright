
export const convertStringArrayToSelectObjectArray = (arr) => {
    return arr.map(v => {
        return { label: v.toLowerCase(), value: v.toLowerCase()}
    });
}

export const convertSelectObjectArrayToStringArray = (arr) => {
    return arr.map( v => {
        return v.label;
    })
}

export const convertSelectObjectToString = (obj) => {
    return obj.label;
}

export const convertStringToSelectObject = (str) => {
    return { label: str, value: str }
}

export const objectOrEmptyString = (obj) => {
    return obj ? obj : "";
}