import {rgbToHex} from "@mui/material";

export const randomColor = () => {
    const [r, g, b] = [Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255)];
    return rgbToHex(`rgb(${r}, ${g}, ${b}`);
}