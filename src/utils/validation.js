export const undefinedOrValue = (val, trim=true) => {
    return val.trim() === "" ? undefined : val.trim();
}

/**
 *
 * @param code
 * @returns {string|null} - null if it is an invalid course code,
 *                          otherwise return formated course code
 */
export const validCourseCode = (code) => {
    const regex = /([a-zA-Z]{2})-?(\d{3,4})/
    const res = regex.exec(code);
    return res ? `${res[1]}-${res[2]}`.toUpperCase() : null
}