import { Outlet } from 'react-router-dom'
import NavBar from "../components/navbar/NavBar";
import {Toaster} from "react-hot-toast";
import { Tooltip } from "react-tooltip";
import 'react-tooltip/dist/react-tooltip.css'
import ConfirmModal from "../components/modal/ConfirmModal";

const Layout = () => {

    return (
        <main className="">
            <ConfirmModal />
            <NavBar />
            <Tooltip anchorId="tooltip" />
            <Toaster/>
            <Outlet/>
        </main>
    )
}

export default Layout;