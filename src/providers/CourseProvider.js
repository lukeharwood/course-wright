import React, {createContext, useContext, useEffect, useState} from "react";
import {useSections} from "../hooks/useSections";
import toast from "react-hot-toast";

export const CourseContext = createContext({})


const CourseProvider = ({children}) => {
    const [focusedDirectory, setFocusedDirectory] = useState("");

    return (
        <CourseContext.Provider value={{focusedDirectory, setFocusedDirectory }}>
            {children}
        </CourseContext.Provider>
    );
}

export default CourseProvider;