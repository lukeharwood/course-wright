import React, {createContext, useContext, useEffect, useState} from "react";
import {useSections} from "../hooks/useSections";
import toast from "react-hot-toast";

export const DashboardContext = createContext({})


const DashboardProvider = ({children}) => {
    const [courses, setCourses] = useState([]);
    const [sections, setSections] = useState([]);
    const { getSections } = useSections();

    useEffect(() => {
        getSections().then(s => setSections(s)).catch(err => toast.error(err));
    }, [courses])
    return (
        <DashboardContext.Provider value={{courses, setCourses, sections, setSections }}>
            {children}
        </DashboardContext.Provider>
    );
}

export default DashboardProvider;