import {ServerPath} from "../data/paths";
import useAxios from "./useAxios";


export const useSections = () => {

    const axios = useAxios();

    const getSections = async () => {
        try {
            const response = await axios.get(ServerPath.Dashboard.SECTIONS);
            return Promise.resolve(response.data?.sections);
        } catch (err) {
            console.log(err);
            return Promise.reject();
        }
    }

    const addSection = async (section) => {
        try {
            const response = await axios.post(ServerPath.Dashboard.SECTIONS, section);
            return Promise.resolve(response.data?.section);
        } catch (err) {
            if (!err?.response) {
                return Promise.reject("Failed to connect.");
            } else if (err?.response === 500) {
                return Promise.reject("Failed to save.");
            }
            console.log(err);
            return Promise.reject();
        }
    }

    const addSections = async (sectionData) => {
        try {
            await axios.post(ServerPath.Dashboard.SECTIONS, sectionData);
            return Promise.resolve();
        } catch (err) {
            if (!err?.response) {
                return Promise.reject("Failed to connect.");
            } else if (err?.response === 500) {
                return Promise.reject("Failed to load resources.");
            }
            console.log(err);
            return Promise.reject();
        }
    }

    return { getSections, addSection, addSections };
}
