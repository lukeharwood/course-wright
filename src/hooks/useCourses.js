import {useEffect, useState} from "react";
import {ServerPath} from "../data/paths";
import toast from "react-hot-toast";
import useAxios from "./useAxios";
import {undefinedOrValue} from "../utils/validation";

export const useCourses = () => {

    const axios = useAxios();

    const createDirectory = () => {

    }

    const createPage = (id, page) => {

    }

    /**
     * Delete a course with the given ID
     * @param id
     * @returns {Promise<void>}
     */
    const deleteCourse = async (id) => {
        try {
            const response = await axios.delete(ServerPath.Dashboard.COURSE_BY_ID(id));
            if (response.status !== 204) {
                return Promise.reject();
            } else {
                return Promise.resolve();
            }
        } catch (err) {
            console.log(err);
            return Promise.reject();
        }
    }

    /**
     * Get the basic metadata from each course related to the user
     * @returns {Promise<unknown>}
     */
    const getCourses = async () => {
        try {
            const response = await axios.get(ServerPath.Dashboard.GET_COURSES);
            const { courses } = response.data;
            return Promise.resolve(courses);
        } catch (err) {
            console.log(err);
            return Promise.reject();
        }
    }

    /**
     * Retrieve the basic meta data of a course
     * @param id
     * @returns {Promise<unknown>}
     */
    const getCourse = async (id) => {
        try {
            const response = await axios.get(ServerPath.Dashboard.COURSE_BY_ID(id));
            const { course } = response.data;
            return Promise.resolve(course);
        } catch (err) {
            console.log(err);
            return Promise.reject();
        }
    }

    /**
     * Update a course with the given ID
     * @param id
     * @param color
     * @param name
     * @param code
     * @param tags
     * @param subject
     * @param description
     * @param visibility
     * @param shareOption
     * @returns {Promise<unknown>}
     */
    const updateCourse = async (id, { color, name, code, tags, subject, description, visibility, shareOption}) => {
        const data = {
            color,
            name,
            code: undefinedOrValue(code),
            tags,
            subject: undefinedOrValue(subject),
            description: undefinedOrValue(description),
            visibility,
            shareOption
        }
        try {
            const response = await axios.patch(ServerPath.Dashboard.CREATE_EDIT_COURSE(id), data);
            if (response.status === 200 && response.data?.course) {
                return Promise.resolve(response.data.course);
            } else {
                return Promise.reject("Sorry, there was an issue... try again.");
            }
        } catch (err) {
            // TODO: remove all generic rejections and log them somewhere
            console.log(err);
            return Promise.reject();
        }
    }

    return {getCourses, getCourse, updateCourse, deleteCourse};
}
