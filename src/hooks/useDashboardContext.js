import {useContext} from "react";
import {DashboardContext} from "../providers/DashboardProvider";

const useDashboardContext = () => {
    return useContext(DashboardContext);
}

export default useDashboardContext;