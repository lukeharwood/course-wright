import {useContext} from "react";
import {CourseContext} from "../providers/CourseProvider";

const useDashboardContext = () => {
    return useContext(CourseContext);
}

export default useDashboardContext;